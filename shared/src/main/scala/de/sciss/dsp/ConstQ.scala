/*
 * ConstQ.scala
 * (ScissDSP)
 *
 * Copyright (c) 2001-2021 Hanns Holger Rutz. All rights reserved.
 *
 * This software is published under the GNU Affero General Public License v3+
 *
 *
 * For further information, please contact Hanns Holger Rutz at
 * contact@sciss.de
 */

package de.sciss.dsp

import de.sciss.serial.{ConstFormat, DataInput, DataOutput}

import scala.language.implicitConversions

object ConstQ {

  sealed trait ConfigLike {
    /** Sampling rate of the audio material, in Hertz. */
    def sampleRate: Double

    /** Lowest frequency in the logarithmic spectral, in Hertz. */
    def minFreq: Double

    /** Highest frequency in the logarithmic spectral, in Hertz. */
    def maxFreq: Double

    /** Maximum temporal resolution in milliseconds. This resolution is achieved for high frequencies. */
    def maxTimeRes: Double

    /** Maximum size of FFTs calculated. This constraints the actual bandwidth of the minimum frequency
      * spectral resolution. Even if the bands per octave and minimum frequency would suggest a higher
      * FFT size for low frequencies, this setting prevents these, and therefore constraints processing
      * time.
      */
    def maxFFTSize: Int

    /** Number of frequency bands resolved per octave. */
    def bandsPerOct: Int

    /** Decimal number of frequency bands resolved per octave. */
    def bandsPerOctF: Double

    /** Number of kernels, or zero to derive from bands-per-oct. */
    def numKernels: Int

    /** Whether to weigh the kernels with the reciprocal of the estimated
      * broadband energy captured. Defaults to `true`.
      */
    def energyWeighted: Boolean

    /** Policy regarding parallelization of the calculation. */
    def threading: Threading
  }

  object Config {
    implicit def build(b: ConfigBuilder): Config = b.build

    def apply(): ConfigBuilder = new ConfigBuilderImpl

    private final val COOKIE_OLD  = 0x4352  // was "CQ"
    private final val COOKIE      = 0x4353

    implicit object format extends ConstFormat[Config] {
      def write(v: Config, out: DataOutput): Unit = {
        import v._
        out.writeShort(COOKIE)
        out.writeDouble(sampleRate)
        out.writeDouble(minFreq)
        out.writeDouble(maxFreq)
        out.writeDouble(maxTimeRes)
        out.writeInt(maxFFTSize)
        out.writeDouble(bandsPerOctF)
        out.writeInt(numKernels)
        out.writeBoolean(energyWeighted)
        Threading.format.write(threading, out)
      }

      def read(in: DataInput): Config = {
        val cookie        = in.readShort()
        val isNew         = cookie == COOKIE
        require(isNew || cookie == COOKIE_OLD, s"Unexpected cookie $cookie")
        val sampleRate    = in.readDouble()
        val minFreq       = in.readDouble()
        val maxFreq       = in.readDouble()
        val maxTimeRes    = in.readDouble()
        val maxFFTSize    = in.readInt()
        val bandsPerOct   = if (isNew) 0 else in.readInt()
        val bandsPerOctF  = if (isNew) in.readDouble() else bandsPerOct.toDouble
        val numKernels    = if (isNew) in.readInt() else {
          getNumKernelsF(bandsPerOct = bandsPerOctF, minFreq = minFreq, maxFreq = maxFreq)
        }
        val energyWeights = if (isNew) in.readBoolean() else true
        val threading     = Threading.format.read(in)
        ConfigImpl(
          sampleRate    = sampleRate,
          minFreq       = minFreq,
          maxFreq       = maxFreq,
          maxTimeRes    = maxTimeRes,
          maxFFTSize    = maxFFTSize,
          bandsPerOctF  = bandsPerOctF,
          numKernels    = numKernels,
          energyWeighted = energyWeights,
          threading     = threading
        )
      }
    }
  }

  sealed trait Config extends ConfigLike

  object ConfigBuilder {
    def apply(config: Config): ConfigBuilder = {
      import config._
      val b = new ConfigBuilderImpl
      b.sampleRate    = sampleRate
      b.minFreq       = minFreq
      b.maxFreq       = maxFreq
      b.maxTimeRes    = maxTimeRes
      b.maxFFTSize    = maxFFTSize
      b.bandsPerOctF  = bandsPerOctF
      b.numKernels    = numKernels
      b.threading     = threading
      b
    }
  }

  sealed trait ConfigBuilder extends ConfigLike {
    var sampleRate    : Double
    var minFreq       : Double
    var maxFreq       : Double
    var maxTimeRes    : Double
    var maxFFTSize    : Int
    /** Convenient setter for integer number of bands per octave.
      * This is synchronized with the decimal resolution `bandsPerOctF`.
      */
    var bandsPerOct   : Int
    var bandsPerOctF  : Double

    /** If the number of kernels is explicitly set,
      * it will be directly used by the algorithm, and `bandsPerOctF`
      * will be recalculated based on the number of kernels, minimum and maximum frequency.
      * If it is not set (default), it will be calculated automatically based on
      * `bandsPerOctF`, minimum and maximum frequency.
      */
    var numKernels    : Int

    var energyWeighted : Boolean

    var threading     : Threading

    def build: Config
  }

  private final class ConfigBuilderImpl extends ConfigBuilder {
    override def toString = s"ConstQ.ConfigBuilder@${hashCode.toHexString}"

    // rather moderate defaults with 55 Hz, 8ms spacing, 4096 FFT...
    var sampleRate    : Double    = 44100.0
    var maxTimeRes    : Double    = 8.0
    var maxFFTSize    : Int       = 4096
    var energyWeighted: Boolean   = true
    var threading     : Threading = Threading.Multi

    private[this] var _bandsPerOctF : Double  = 24
    private[this] var _minFreq      : Double  = 55.0
    private[this] var _maxFreq      : Double  = 20000.0
    private[this] var _numKernels   : Int     = 0  // unset by default

    def numKernels: Int = _numKernels

    private def fromKernel(): Unit =
      if (_numKernels != 0) {
        _bandsPerOctF = getBandsPerOct(_numKernels, maxFreq = _maxFreq, minFreq = _minFreq)
      }

    def numKernels_=(value: Int): Unit = {
      _numKernels = value
      fromKernel()
    }

    def bandsPerOctF: Double = _bandsPerOctF

    def bandsPerOctF_=(value: Double): Unit = {
      _bandsPerOctF = value
      _numKernels   = 0
    }

    def minFreq: Double = _minFreq

    def minFreq_=(value: Double): Unit = {
      _minFreq = value
      fromKernel()
    }

    def maxFreq: Double = _maxFreq

    def maxFreq_=(value: Double): Unit = {
      _maxFreq = value
      fromKernel()
    }

    def bandsPerOct: Int = math.round(bandsPerOctF).toInt

    def bandsPerOct_=(value: Int): Unit =
      bandsPerOctF = value.toDouble

    def build: Config = {
      val numK = if (_numKernels != 0) _numKernels else {
        getNumKernelsF(_bandsPerOctF, minFreq = _minFreq, maxFreq = _maxFreq)
      }
      ConfigImpl(
        sampleRate    = sampleRate,
        minFreq       = _minFreq,
        maxFreq       = _maxFreq,
        maxTimeRes    = maxTimeRes,
        maxFFTSize    = maxFFTSize,
        bandsPerOctF  = _bandsPerOctF,
        numKernels    = numK,
        energyWeighted= energyWeighted,
        threading     = threading
      )
    }
  }

  private final case class ConfigImpl(sampleRate: Double, minFreq: Double, maxFreq: Double, maxTimeRes: Double,
                                      maxFFTSize: Int, bandsPerOctF: Double, numKernels: Int,
                                      energyWeighted: Boolean,
                                      threading: Threading)
    extends Config {

    def bandsPerOct: Int = math.round(bandsPerOctF).toInt

    override def toString = s"ConstQ.Config@${hashCode.toHexString}"
  }

  /** Calculates the number of kernels resulting from a given setting. */
  def getNumKernelsF(bandsPerOct: Double, maxFreq: Double, minFreq: Double): Int =
    math.ceil(bandsPerOct * Util.log2(maxFreq / minFreq)).toInt

  def getNumKernels(bandsPerOct: Int, maxFreq: Double, minFreq: Double): Int =
    getNumKernelsF(bandsPerOct.toDouble, maxFreq = maxFreq, minFreq = minFreq)

  def getBandsPerOct(numKernels: Int, maxFreq: Double, minFreq: Double): Double =
    numKernels / Util.log2(maxFreq / minFreq)

  def apply(config: Config = Config().build): ConstQ = createInstance(config)

  private final class Impl(val config: Config, kernels: Array[Kernel], val fft: Fourier, val fftBuffer: Array[Double])
    extends ConstQ {

    val numKernels: Int   = kernels.length
    val fftSize   : Int   = fft.size

    override def toString = s"ConstQ@${hashCode.toHexString}"

    def getFrequency(kernel: Int): Double = kernels(kernel).freq

    def transform(input: Array[Double], inLen: Int, out0: Array[Double], inOff: Int, outOff: Int): Array[Double] = {
      val output = if (out0 == null) new Array[Double](numKernels) else out0

      val off = fftSize >> 1
      val num = math.min(fftSize - off, inLen)

      System.arraycopy(input, inOff, fftBuffer, off, num)
      var i = off + num
      while (i < fftSize) {
        fftBuffer(i) = 0f
        i += 1
      }
      val num2 = math.min(fftSize - num, inLen - num)
      System.arraycopy(input, inOff + num, fftBuffer, 0, num2)
      i = num2
      while (i < off) {
        fftBuffer(i) = 0f
        i += 1
      }

      fft.realForward(fftBuffer)

      convolve(output, outOff)
    }

    def convolve(out0: Array[Double], outOff0: Int): Array[Double] = {
      val output = if (out0 == null) new Array[Double](numKernels) else out0

      var k = 0; var outOff = outOff0; while (k < numKernels) {
        val kern = kernels(k)
        val data = kern.data
        var f1 = 0.0
        var f2 = 0.0
        var i = kern.offset; var j = 0; while (j < data.length) {
          // complex mult: a * b =
          // (re(a)re(b)-im(a)im(b))+i(re(a)im(b)+im(a)re(b))
          // ; since we left out the conjugation of the kernel(!!)
          // this becomes (assume a is input and b is kernel):
          // (re(a)re(b)+im(a)im(b))+i(im(a)re(b)-re(a)im(b))
          // ; in fact this conjugation is unimportant for the
          // calculation of the magnitudes...
          val re1 = fftBuffer(i)
          val im1 = fftBuffer(i + 1)
          val re2 = data(j)
          val im2 = data(j + 1)
          f1 += re1 * re2 - im1 * im2
          f2 += re1 * im2 + im1 * re2
          i += 2; j += 2
        }
        //			cBuf[ k ] = ;  // squared magnitude
        //			f1 = (float) ((Math.log( f1 * f1 + f2 * f2 ) + LNKORR_ADD) * gain);
        //f1 = Math.max( -160, f1 + 90 );

        // since we use constQ to decimate spectra, we actually
        // are going to store a "mean square" of the amplitudes
        // so at the end of the decimation we'll have one square root,
        // or even easier, when calculating the logarithm, the
        // multiplier of the log just has to be divided by two.
        // hence we don't calc abs( f ) but abs( f )^2 !

        output(outOff) = f1 * f1 + f2 * f2

        k += 1; outOff += 1
      }

      output
    }
  }

  private def createInstance(config0: Config): Impl = {
    val fs = config0.sampleRate
    val config = if (config0.maxFreq <= fs / 2) config0
    else {
      val c = ConfigBuilder(config0)
      c.maxFreq = fs/2
      c.build
    }

    val q           = 1.0 / (math.pow(2.0, 1.0 / config.bandsPerOctF) - 1.0)
    val numKernels  = config.numKernels
    val kernels     = new Array[Kernel](numKernels)
    //		cqKernels	= new float[ cqKernelNum ][];
    //		cqKernelOffs= new int[ cqKernelNum ];
    val qN          = q * config.sampleRate
    val maxKernLen  = qN / config.minFreq
    val minKernLen  = qN / config.maxFreq

    val fftSize     = math.min(config.maxFFTSize, Util.nextPowerOfTwo(math.ceil(maxKernLen).toInt))
    val fftSizeC    = fftSize << 1
    val fftBuf      = new Array[Double](fftSizeC)
    val fft         = Fourier(fftSize, config.threading)

    //		thresh		= 0.0054f / fftLen; // for Hamming window
    // weird observation : lowering the threshold will _increase_ the
    // spectral noise, not improve analysis! so the truncating of the
    // kernel is essential for a clean analysis (why??). the 0.0054
    // seems to be a good choice, so don't touch it.
    //		threshSqr	= 2.916e-05f / (fftSize * fftSize); // for Hamming window (squared!)
    //		threshSqr	= 2.916e-05f / fftSize; // for Hamming window (squared!)
    // (0.0054 * 3).squared

//    val threshSqr = 2.6244e-4f / (fftSize * fftSize) // for Hamming window (squared!)

//    println(f"q = $q%1.2f; minKernLen = $minKernLen%1.2f; maxKernLen = $maxKernLen%1.2f; fftSize = $fftSize")

    val enW     = config.energyWeighted
    // the factor is empirically chosen so that a 1 kHz sine tone
    // comes out with the same peak amplitude compared to a linear
    // FFT analysis
    val weight0 = if (enW) 3.09 / fftSize else 0.258 / (fftSize * minKernLen)

//    val MAG_BUF_SQ      = new Array[Array[Double]](numKernels)
//    val MAG_BUF_START   = new Array[Int](numKernels)
//    val MAG_BUF_STOP    = new Array[Int](numKernels)
//    val MAG_BUF_CENTER  = new Array[Int](numKernels)
//    val NUM_MAG         = (fftSize >> 1) + 1

    var ki = 0
    while (ki < numKernels) {
      val idealKernLen  = maxKernLen * math.pow(2, (-ki).toDouble / config.bandsPerOctF) // toDouble
      val idealKernLenI = math.ceil(idealKernLen).toInt
      val kernelLen     = math.min(fftSize, idealKernLenI)
      val kernelLenE    = kernelLen & ~1
      val win           = Window.Hamming.create(kernelLen)

      val centerFreq    = config.minFreq * math.pow(2, ki.toDouble / config.bandsPerOctF)
      val centerFreqN0  = centerFreq / fs
      val centerFreqN   = centerFreqN0 * -Util.Pi2
      val centerOff     = math.round(centerFreqN0 * fftSize).toInt << 1
//      println(f"For k $ki: fc = $centerFreq%1.1f, kernelLen is $kernelLen, ideal $idealKernLen%1.1f, centerOff $centerOff")

      //			weight		= 1.0f / (kernelLen * fftSize);
      // this is a good approximation in the kernel len truncation case
      // (tested with pink noise, where it will appear pretty much
      // with the same brightness over all frequencies)

//      val weight = if (enW) weight0 / (idealKernLen + kernelLen) else weight0
      val weight = if (enW) {
        val geom = if (idealKernLenI == kernelLen) kernelLen.toDouble else math.sqrt(idealKernLen * kernelLen)
        weight0 / geom
      } else {
        weight0
      }

      //			weight = 2 / ((theorKernLen + kernelLen) * Math.sqrt( fftSize ));

      {
        var m = kernelLenE
        val n = fftSizeC - kernelLenE
        while (m < n) {
          fftBuf(m) = 0f
          m += 1
        }
      }

      // note that we calculate the complex conjugation of
      // the temporalKernal and reverse its time, so the resulting
      // FFT can be immediately used for the convolution and does not
      // need to be conjugated; this is due to the Fourier property
      // h*(-x) <-> H*(f). time reversal is accomplished by
      // having iteration variable j run....

      // note: in the old FFT algorithm, there was a mistake, whereby complex input
      // data was already time reversed in the transform. this is corrected now.

      //			for( int i = kernelLen - 1, j = fftSizeC - kernelLenE; i >= 0; i-- ) { ... }
      //			for( int i = 0, j = 0; /* fftSizeC - kernelLenE; */ i < kernelLen; i++ ) { ... }
      //         var i = 0; var j = fftSizeC - kernelLenE; while( i < kernelLen ) {

      {
        var i = kernelLen - 1
        var j = fftSizeC - kernelLenE
        while (i >= 0) {
          // complex exponential of a purely imaginary number
          // is cos( imag( n )) + i sin( imag( n ))
          val d1    = centerFreqN * i
          //				d1			= centerFreqN * (i - kernelLenE);
          val cos   = math.cos(d1)
          val sin   = math.sin(d1) // Math.sqrt( 1 - cos*cos );
          val d2    = win(i) * weight
          fftBuf(j) = d2 * cos; j += 1
          //				fftBuf[ j++ ] = -f1 * sin;  // conj!
          fftBuf(j) = d2 * sin; j += 1 // NORM!
          if (j == fftSizeC) j = 0
          //            i += 1 }
          i -= 1
        }
      }

      // XXX to be honest, i don't get the point
      // of calculating the fft here, since we
      // have an analytic description of the kernel
      // function, it should be possible to calculate
      // the spectral coefficients directly
      // (the fft of a hamming is a gaussian,
      // isn't it?)

      //			Fourier.complexTransform( fftBuf, fftSize, Fourier.FORWARD )

      //println( fftBuf.mkString( "[ ", ", ", " ]" ))
      fft.complexForward(fftBuf)
      //val _out  = fftBuf.clone()
      //Complex.rect2Polar( _out, 0, _out, 0, _out.length )
      //val _test = _out.zipWithIndex.collect { case (f, i) if i % 2 == 0 => f }
      //println( _test.mkString( "[ ", ", ", " ]" ))

      // with a "high" threshold like 0.0054, the
      // point is _not_ to create a sparse matrix by
      // gating the values. in fact we can locate
      // the kernel spectrally, so all we need to do
      // is to find the lower and upper frequency
      // of the transformed kernel! that makes things
      // a lot easier anyway since we don't need
      // to employ a special sparse matrix library.
//      val specStart = {
//        var i = 0; var break = false; while (!break && i <= fftSize) {
//          val f1      = fftBuf(i)
//          val f2      = fftBuf(i + 1)
//          val magSqr  = f1 * f1 + f2 * f2
//          if (magSqr > threshSqr) break = true else i += 2
//        }
//        i
//      }

      // Oct 2021: we use a better technique to determine
      // the kernel support: The hamming window'ed kernels
      // exhibit no ripple in the main lobe, and a clear
      // and step first notch before going into the side-band
      // ripples. Therefore we can search for the first
      // local minima below and above the center bin.

      val magSqrCenter = {
        val f1 = fftBuf(centerOff)
        val f2 = fftBuf(centerOff + 1)
        f1 * f1 + f2 * f2
      }

      val specStart = {
        var i  = centerOff - 2  // begin one to the left of the center
        var m1 = magSqrCenter   // previous
        var m2 = magSqrCenter   // previous of previous
        var continue = true
        while (continue && i >= 0) {
          val f1  = fftBuf(i)
          val f2  = fftBuf(i + 1)
          val m0  = f1 * f1 + f2 * f2
          if (m0 > m1 && m1 < m2) { // find first local minimum
            continue = false
          } else {
            m2 = m1
            m1 = m0
            i -= 2
          }
        }
        i + 2 // location is one to the right
      }

      // final matrix product:
      // input chunk (fft'ed) is a row vector with n = fftSize
      // kernel is a matrix mxn with m = fftSize, n = numKernels
      // result is a row vector with = numKernels
      // ; note that since the input is real and hence we
      // calculate only the positive frequencies (up to pi),
      // we might need to mirror the input spectrum to the
      // negative frequencies. however it can be observed that
      // for practically all kernels their frequency bounds
      // lie in the positive side of the spectrum (only the
      // high frequencies near nyquist blur across the pi boundary,
      // and we will cut the overlap off by limiting specStop
      // to fftSize instead of fftSize<<1 ...).

//      val specStop = {
//        var i = specStart; var break = false; while (!break && i <= fftSize) {
//          val f1      = fftBuf(i)
//          val f2      = fftBuf(i + 1)
//          val magSqr  = f1 * f1 + f2 * f2
//          if (magSqr <= threshSqr) break = true else i += 2
//        }
//        i
//      }

      val specStop = {
        var i  = centerOff + 2  // begin one to the right of the center
        var m1 = magSqrCenter   // previous
        var m2 = magSqrCenter   // previous of previous
        var continue = true
        while (continue && i <= fftSize) {
          val f1  = fftBuf(i)
          val f2  = fftBuf(i + 1)
          val m0  = f1 * f1 + f2 * f2
          if (m0 > m1 && m1 < m2) { // find first local minimum
            continue = false
          } else {
            m2 = m1
            m1 = m0
            i += 2
          }
        }
        i - 2 // location is one to the left
      }

//      val MAG_BUF = {
//        val b = new Array[Double](NUM_MAG)
//        var j = 0
//        var i = 0
//        while (i <= fftSize) {
//          val f1      = fftBuf(i)
//          val f2      = fftBuf(i + 1)
//          val magSqr  = f1 * f1 + f2 * f2
//          b(j) = magSqr
//          i += 2
//          j += 1
//        }
//        b
//      }

//      MAG_BUF_SQ    (ki) = MAG_BUF
//      MAG_BUF_START (ki) = specStart >> 1
//      MAG_BUF_STOP  (ki) = specStop  >> 1
//      MAG_BUF_CENTER(ki) = centerOff >> 1

//      println(f"Kernel $ki : specStart $specStart; specStop $specStop; len ${specStop - specStart}, centerFreq $centerFreq%1.1f")
      kernels(ki) = new Kernel(specStart, new Array[Double](specStop - specStart), centerFreq)
      System.arraycopy(fftBuf, specStart, kernels(ki).data, 0, specStop - specStart)
      ki += 1
    }

//    java.awt.EventQueue.invokeLater(() => {
//      new INSPECT(MAG_BUF_SQ, MAG_BUF_START, MAG_BUF_STOP, MAG_BUF_CENTER, NUM_MAG)
//    })

    new Impl(config, kernels, fft, fftBuf)
  }

/*

  // a debugging view to ensure we capture the correct kernels

  private class INSPECT(MAG_BUF_SQ    : Array[Array[Double]],
                        MAG_BUF_START : Array[Int],
                        MAG_BUF_STOP  : Array[Int],
                        MAG_BUF_CENTER: Array[Int],
                        numMag: Int,
                       ) {
    import java.awt.geom.{Line2D, Path2D}
    import java.awt.{BorderLayout, Color, Dimension, FlowLayout, Graphics, Graphics2D}
    import javax.swing.event.ChangeEvent
    import javax.swing.{JComponent, JFrame, JLabel, JPanel, JSpinner, SpinnerNumberModel, WindowConstants}

    val numKernels: Int = MAG_BUF_SQ.length
    assert (MAG_BUF_START .length == numKernels)
    assert (MAG_BUF_STOP  .length == numKernels)
    assert (MAG_BUF_CENTER.length == numKernels)
    val mKernel   = new SpinnerNumberModel(0, 0, numKernels - 1, 1)
    val ggKernel  = new JSpinner(mKernel)
    mKernel.addChangeListener((_: ChangeEvent) => c.repaint())
    val mTop      = new SpinnerNumberModel(numMag, 2, numMag, 1)
    val ggTop     = new JSpinner(mTop)
    mTop.addChangeListener((_: ChangeEvent) => c.repaint())

    object c extends JComponent {
      setPreferredSize(new Dimension(640, 480))

      override def paintComponent(g: Graphics): Unit = {
        val ki    = mKernel.getNumber.intValue()
        val b     = MAG_BUF_SQ    (ki)
        val start = MAG_BUF_START (ki)
        val stop  = MAG_BUF_STOP  (ki)
        val center= MAG_BUF_CENTER(ki)

        val g2 = g.asInstanceOf[Graphics2D]
        g2.setColor(Color.white)
        val w = getWidth
        val h = getHeight
        g2.fillRect(0, 0, w, h)
        g2.setColor(Color.black)
        val p = new Path2D.Double()
        var bMin = Double.PositiveInfinity
        var bMax = Double.NegativeInfinity
        val top  = mTop.getNumber.intValue()
        var i = 0
        while (i < top) {
          val v = math.max(-100.0, math.log10(b(i)))
          if (v < bMin) bMin = v
          if (v > bMax) bMax = v
          i += 1
        }
        println(f"bMin $bMin%1.1f, bMax $bMax%1.1f, start $start, stop $stop, center $center")
        val wm = w - 1
        val hm = h - 1
        val sx = wm.toDouble / (top - 1)
        i = 0
        while (i < top) {
          val v = math.max(-100.0, math.log10(b(i)))
          val v0  = (v - bMin) / (bMax - bMin)
          assert (v0 >= 0 && v0 <= 1, v0.toString)
          val y   = (1.0 - v0) * (h - 1)
          val x   = i * sx
          // println(f"$x%1.1f, $y%1.1f")
          if (i == 0) p.moveTo(x, y) else p.lineTo(x, y)
          i += 1
        }
        g2.draw(p)
        g2.setColor(Color.red)
        val ln = new Line2D.Double
        ln.setLine(start * sx, 0, start * sx, hm)
        g2.draw(ln)
        ln.setLine(stop * sx, 0, stop * sx, hm)
        g2.draw(ln)
        g2.setColor(Color.blue)
        ln.setLine(center * sx, 0, center * sx, hm)
        g2.draw(ln)
      }
    }

    val panel = new JPanel(new BorderLayout())
    val topP = new JPanel(new FlowLayout())
    topP.add(new JLabel("Kernel:"))
    topP.add(ggKernel)
    topP.add(new JLabel("Display Width:"))
    topP.add(ggTop)
    panel.add(topP, BorderLayout.NORTH)
    panel.add(c, BorderLayout.CENTER)

    new JFrame() {
      setContentPane(panel)
      setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)
      pack()
      setLocationRelativeTo(null)
      setVisible(true)
    }
  }
*/

  private final class Kernel(val offset: Int, val data: Array[Double], val freq: Double)

}

trait ConstQ {
  def config: ConstQ.Config

  /** The number of kernels is the total number of frequency bands calculated. */
  def numKernels: Int

  /** The actual maximum FFT size used. */
  def fftSize: Int

  /** The buffer used to perform FFTs. */
  def fftBuffer: Array[Double]

  /** Queries a kernel frequency in Hertz.
    *
    * @param   kernel   the kernel index, from `0` up to and including `numKernels-1`
    */
  def getFrequency(kernel: Int): Double

  /** Transforms a time domain input signal to obtain the constant Q spectral coefficients.
    *
    * '''Note:''' these are "power" coefficients, so they should be treated like squared
    * amplitudes. To transform them to decibels use `10 * log10(c)`.
    *
    * @param input   the time domain signal, which will be read from `inOff` for `inLen` samples
    * @param inOff   the offset into the input array
    * @param inLen   the number of samples to take from the input array
    * @param output  the target kernel buffer (size should be at least `outOff` + `numKernels`.
    *                if `null` a new buffer is allocated
    * @param outOff  the offset into the output buffer
    * @return  the output buffer which is useful when the argument was `null`
    */
  def transform(input: Array[Double], inLen: Int, output: Array[Double], inOff: Int = 0, outOff: Int = 0): Array[Double]

  /** Assumes that the input was already successfully transformed
    * into the Fourier domain (namely into fftBuf as returned by
    * `fftBuffer`. From this FFT, the method calculates the
    * convolutions with the filter kernels, returning the magnitudes
    * of the filter outputs.
    *
    * @param output  the target kernel buffer (size should be at least
    *                `outOff` + `numKernels`. if `null` a new buffer is allocated
    * @param outOff  the offset into the output buffer
    * @return  the output buffer which is useful when the argument was `null`
    */
  def convolve(output: Array[Double], outOff: Int = 0): Array[Double]
}