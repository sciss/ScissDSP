package de.sciss.dsp

object ConstQTest extends App with Runnable {
  run()

  def run(): Unit = {
    val qc  = ConstQ.Config()
//    qc.minFreq = 1.0
//    qc.maxFreq = qc.sampleRate/2
//    qc.numKernels = 10
//    qc.maxFFTSize = 32
    val q   = ConstQ(qc)
    val in  = Array.fill(1024)(util.Random.nextDouble() * 2 - 1)
    val out = q.transform(in, in.length, null)
    println(out.take(10).toIndexedSeq)
  }
}
