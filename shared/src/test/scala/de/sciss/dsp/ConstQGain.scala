package de.sciss.dsp

import de.sciss.transform4s.fft.DoubleFFT_1D

object ConstQGain extends App with Runnable {
  run()

  def run(): Unit = {
    val qc  = ConstQ.Config()
    qc.sampleRate = 24000.0
    val N = 8192 // 65536 // * 2
    val M = 1024 // 1024
    val F = 1000.0
    val A = 0.25
    qc.maxFFTSize = N
    qc.minFreq    = 10.0
    qc.maxFreq    = qc.sampleRate/2
    qc.numKernels = M
//    qc.energyWeighted = false
    val SR  = qc.sampleRate
    val q   = ConstQ(qc)
    println(s"num-per-oct ${q.config.bandsPerOctF}")
    val fN  = F * math.Pi * 2 / SR
    val in  = Array.tabulate(N)(i => math.sin(i * fN) * A)
    val qOut = q.transform(in, N, null)
    val qMaxASq = qOut.max
    val qMaxA = math.sqrt(qMaxASq)
    val qMaxK = qOut.indexOf(qMaxASq)
    val qMaxF = q.getFrequency(qMaxK)
    println(f"ConstQ: Maximum of $qMaxA%1.6f or ${math.log10(qMaxA) * 20}%1.2f dB at kernel index $qMaxK or frequency $qMaxF%4.1f Hz")

    val fft = DoubleFFT_1D(N)
    fft.realForward(in)
    val fOut  = (in.sliding(1, 2) zip in.tail.sliding(1, 2)).map { case (Array(re), Array(im)) =>
      math.sqrt(re * re + im * im) / (N/2)
    } .toArray
    val fMaxA = fOut.max
    val fMaxK = fOut.indexOf(fMaxA)
    val fMaxF = fMaxK * SR / N
    println(f"FFT   : Maximum of $fMaxA%1.6f or ${math.log10(fMaxA) * 20}%1.2f dB at kernel index $fMaxK or frequency $fMaxF%4.1f Hz")

    println(f"Ratio: $fMaxA%1.6f/$qMaxA%1.6f")

    /*

    energy-weighted:

    Seq(
      0.827099/0.267343,
      0.826849/0.267346,
      0.827020/0.267351,
      0.826957/0.267348,
    ).mean.sqrt ~= 3.09

    Seq(
      0.827099/3.210654,
      0.826849/3.210696,
      0.827020/3.210753,
      0.826957/3.210718,
    ).mean ~= 0.258

     */
  }
}
