package de.sciss.dsp

import de.sciss.audiofile.AudioFile

import java.awt.{EventQueue, RenderingHints}

object ConstQWeights extends App with Runnable {
  run()

  def run(): Unit = {
    val qc  = ConstQ.Config()
    val N = 16384 // 32768 // 65536 // * 2
    val M = 1024
    val S = 16
    qc.maxFFTSize = N
    qc.minFreq    = 10.0
    qc.maxFreq    = qc.sampleRate/2
    qc.numKernels = M
    val q   = ConstQ(qc)
    val af  = AudioFile.openRead("/data/temp/pinknoise.aif")
    val numChunks = (af.numFrames / N).toInt
    println(s"numChunks $numChunks")
    val buf = af.buffer(N)
    val in = buf(0)
    val outAvg  = new Array[Double](M)
    val gain = math.sqrt(1.0 / numChunks)
    for (_ <- 0 until numChunks) {
      af.read(buf)
      val out = q.transform(in, N, null)
      for (i <- 0 until M) {
        outAvg(i) += out(i) * gain
      }
    }
    val smooth = Array.tabulate(M) { ki =>
      val s = outAvg.slice(ki - S, ki + S)
      s.sum / s.length
    }

    class INSPECT(b: Array[Double]) {
      import java.awt.geom.Path2D
      import java.awt.{Color, Dimension, Graphics, Graphics2D}
      import javax.swing.{JComponent, JFrame, WindowConstants}

      object c extends JComponent {
        setPreferredSize(new Dimension(640, 480))

        override def paintComponent(g: Graphics): Unit = {
          val g2 = g.asInstanceOf[Graphics2D]
          g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
          g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE)
          g2.setColor(Color.white)
          val w = getWidth
          val h = getHeight
          g2.fillRect(0, 0, w, h)
          g2.setColor(Color.black)
          val p = new Path2D.Double()
          var bMin = Double.PositiveInfinity
          var bMax = Double.NegativeInfinity
          val top  = b.length // mTop.getNumber.intValue()
          var i = 0
          while (i < top) {
            val v = math.max(-320.0, 20.0 * math.log10(b(i)))
            if (v < bMin) bMin = v
            if (v > bMax) bMax = v
            i += 1
          }
          println(f"bMin $bMin%1.1f, bMax $bMax%1.1f")
          val wm = w - 1
//          val hm = h - 1
          val sx = wm.toDouble / (top - 1)
          i = 0
          while (i < top) {
            val v = math.max(-320.0, 20.0 * math.log10(b(i)))
            val v0  = (v - bMin) / (bMax - bMin)
            assert (v0 >= 0 && v0 <= 1, v0.toString)
            val y   = (1.0 - v0) * (h - 1)
            val x   = i * sx
            // println(f"$x%1.1f, $y%1.1f")
            if (i == 0) p.moveTo(x, y) else p.lineTo(x, y)
            i += 1
          }
          g2.draw(p)
        }
      }

      new JFrame() {
        getContentPane.add(c)
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
        pack()
        setLocationRelativeTo(null)
        setVisible(true)
      }
    }

    EventQueue.invokeLater(() => new INSPECT(smooth))
  }
}
